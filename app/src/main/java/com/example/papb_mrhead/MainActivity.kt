package com.example.papb_mrhead

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.ImageView

class MainActivity : AppCompatActivity() {
    private lateinit var checkBoxRambut: CheckBox
    private lateinit var checkBoxAlis: CheckBox
    private lateinit var checkBoxJanggut: CheckBox
    private lateinit var checkBoxKumis: CheckBox
    private lateinit var imageViewRambut: ImageView
    private lateinit var imageViewAlis: ImageView
    private lateinit var imageViewJanggut: ImageView
    private lateinit var imageViewKumis: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Initialize your views
        checkBoxRambut = findViewById(R.id.checkBox)
        checkBoxAlis = findViewById(R.id.checkBox2)
        checkBoxJanggut = findViewById(R.id.checkBox4)
        checkBoxKumis = findViewById(R.id.checkBox3)
        imageViewRambut = findViewById(R.id.HAIR)
        imageViewAlis = findViewById(R.id.EYEBROW)
        imageViewJanggut = findViewById(R.id.BEARD)
        imageViewKumis = findViewById(R.id.MOUSTACHE)

        // Add listeners to the checkboxes
        checkBoxRambut.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                imageViewRambut.visibility = View.VISIBLE
            } else {
                imageViewRambut.visibility = View.INVISIBLE
            }
        }

        checkBoxAlis.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                imageViewAlis.visibility = View.VISIBLE
            } else {
                imageViewAlis.visibility = View.INVISIBLE
            }
        }

        checkBoxJanggut.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                imageViewJanggut.visibility = View.VISIBLE
            } else {
                imageViewJanggut.visibility = View.INVISIBLE
            }
        }

        checkBoxKumis.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                imageViewKumis.visibility = View.VISIBLE
            } else {
                imageViewKumis.visibility = View.INVISIBLE
            }
        }
    }
}
